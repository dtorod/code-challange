const API_URL = 'http://localhost:3001';

export const Api = {
    getAllData: async () => {
        const response = await fetch(`${API_URL}/data`);
        const data = await response.json()
        return data;
    }
}

