import groupBy from 'lodash.groupby'
import { DateTime } from 'luxon'
import { TimeSlot, TimeSlotsByCompany } from "../Interfaces"

export function groupTimeSlotsByDays(timeSlotsByCompany: TimeSlotsByCompany[]) {
    return timeSlotsByCompany.map((companyData: any) => {
      // Group by weekday
      const groupedByWeekDay = groupBy(companyData.time_slots, (timeSlot : TimeSlot) => {
        const weekDay = DateTime.fromISO(timeSlot.start_time).weekdayLong
        return weekDay
      })

      companyData.time_slots = groupedByWeekDay
      return companyData;
    })
  }

export function startTimeAndEndTimeToString (timeSlot : TimeSlot) {
    return `${DateTime.fromISO(timeSlot.start_time).toString()} - ${DateTime.fromISO(timeSlot.end_time).toString()}`;
}

export function selectedTimeSlotToWeekdayAndDate (timeSlot : TimeSlot) {
    return `${DateTime.fromISO(timeSlot.start_time).toFormat('EEE - DD')}. From ${DateTime.fromISO(timeSlot.start_time).toFormat('HH:mm')} to ${DateTime.fromISO(timeSlot.end_time).toFormat('HH:mm')}`;
}

export function timeSlotHoursToString (timeSlot : TimeSlot) {
    return `${DateTime.fromISO(timeSlot.start_time).toFormat('HH:mm')} - ${DateTime.fromISO(timeSlot.end_time).toFormat('HH:mm')}`;
}