import { Fragment, useState } from "react"
import {
    startTimeAndEndTimeToString,
    selectedTimeSlotToWeekdayAndDate,
    timeSlotHoursToString
} from '../../utilities/datesFormatter'
import { SelectedTimeSlots } from '../../Interfaces';
import checkedSvg from '../../img/checked.svg';

interface ICompanyTimeSlots { 
    companyData: any; 
    selectedTimesSlots: SelectedTimeSlots; 
    setSelectedTimeSlots: Function 
}

export default function CompanyTimeSlots({ companyData, selectedTimesSlots, setSelectedTimeSlots } : ICompanyTimeSlots) {
    const [selectedTimeSlot, setSelectedTimeSlot] = useState('No Selected Yet')

    function handleSelectedTimeSlot(timeSlot: any) {
        const fullTimeSlotString = startTimeAndEndTimeToString(timeSlot)
        const selectedTimeSlotFormatted = selectedTimeSlotToWeekdayAndDate(timeSlot)

        setSelectedTimeSlot(selectedTimeSlotFormatted)
        setSelectedTimeSlots({
            ...selectedTimesSlots,
            [companyData.id]: fullTimeSlotString
        })
    }

    function handleDeselectTimeSlot() {
        setSelectedTimeSlot('No Selected Yet')
        setSelectedTimeSlots({
            ...selectedTimesSlots,
            [companyData.id]: undefined
        })
    }

    function renderWeekdayTimeSlots(timeSlot: any) {
        const selectedTimeSlotFormatted = selectedTimeSlotToWeekdayAndDate(timeSlot)
        const timeSlotHoursString = timeSlotHoursToString(timeSlot)
        const fullTimeSlotString = startTimeAndEndTimeToString(timeSlot)

        if (selectedTimeSlot === selectedTimeSlotFormatted) {
            return (
                <li
                    className="time_slot--selected"
                    onClick={() => handleDeselectTimeSlot()}
                    key={timeSlotHoursString}
                >
                    <span>{timeSlotHoursString}</span>
                    <img src={checkedSvg} alt="Checked Icon" />
                </li>)
        }

        if (Object.values(selectedTimesSlots).includes(fullTimeSlotString)) {
            return <li className="time_slot--disabled" key={timeSlotHoursString}>{timeSlotHoursString}</li>
        }

        return (
            <li key={timeSlot.start_time} onClick={() => handleSelectedTimeSlot(timeSlot)}>
                {timeSlotHoursString}
            </li>
        )
    }

    return (
        <div className="company_column_wrapper">
            <h2>{companyData.name}</h2>

            <div className="company_column_reservation_time">
                <h4>Reservation</h4>
                <h3 className="time_slot_reserved">{selectedTimeSlot}</h3>
            </div>
           
            <h4 className="company_column_availability_label">Available times</h4>
            <div className="time_slots_wrapper">
                {Object.keys(companyData.time_slots).map((weekday: string) => (
                    <Fragment key={weekday}>
                        <h4>{weekday}</h4>
                        <ul>
                            {companyData.time_slots[weekday].map((timeSlot: any) =>
                                renderWeekdayTimeSlots(timeSlot)
                            )}
                        </ul>
                    </Fragment>
                ))}
            </div>
        </div>
    )
}