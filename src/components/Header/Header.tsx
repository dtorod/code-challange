import logoSvg from '../../img/logo.svg';
export default function Header() {
    return (
        <div className="header-wrapper">
            <img src={logoSvg} alt="Aroundhome Logo" />
        </div>
    )
}