export interface SelectedTimeSlots {
    [id : number] : string | undefined
}

export interface TimeSlot {
    start_time: string;
    end_time: string;
}

export interface TimeSlotsByCompany {
    id: number;
    name: string;
    type: string;
    time_slots: [ TimeSlot ];
}