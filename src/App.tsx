import { useState, useEffect } from 'react';
import { Api } from './api';
import { groupTimeSlotsByDays } from './utilities/datesFormatter';
import Header from './components/Header';
import CompanyTimeSlots from './components/CompanyTimeSlots';
import { SelectedTimeSlots } from './Interfaces';
import './App.css';

function App() {
  const initialSelectedTimesSlots: SelectedTimeSlots = {
    '1': undefined,
    '2': undefined,
    '3': undefined
  }

  const [timeSlots, setTimeSlots]: [any[], Function] = useState([])
  const [selectedTimesSlots, setSelectedTimeSlots] = useState(initialSelectedTimesSlots)

  useEffect(() => {
    async function loadTimeSlotsData() {
      const allTimeSlots = await Api.getAllData();
      const timeSlotsByWeekday = groupTimeSlotsByDays(allTimeSlots)
      setTimeSlots(timeSlotsByWeekday)
    }

    loadTimeSlotsData()
  }, [])


  return (
    <div>
      <Header />
      <div className="columns_wrapper">
        {timeSlots.map((companyData: any) =>
          <CompanyTimeSlots
            companyData={companyData}
            key={companyData.id}
            selectedTimesSlots={selectedTimesSlots}
            setSelectedTimeSlots={setSelectedTimeSlots}
          />
        )}
      </div>
    </div>
  );
}

export default App;
