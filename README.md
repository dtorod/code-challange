# Aroundhome Code Challenge

This is a Code Challenge for Around Home.

## Available Scripts

To run the project:

### `npm install`

Install all the dependencies required to run the app

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.